package agenda_proyecto_final_barroso;

import Controlador.ControladorLogin;
import Modelo.ModeloLogin;
import Vista.VistaLogin;
import java.awt.EventQueue;

public class Agenda_Proyecto_Final_Barroso {

    public static void main(String[] args) {
        
        /*Comenzamos mandando los objetos de vista y modelo a el contructor
        de ControladorLogin para que pueda mostrar la vista y pueda trabajar*/
        
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                /* Mandando VistaLogin y ModeloLogin a el constructor de el ControladorLogin
                e iniciando el metodo iniciar deControladorLogin*/
                new ControladorLogin(new VistaLogin(), new ModeloLogin()).iniciar();
            }
        });
    }
    
}
