package Modelo;

import Singleton.Singleton;
import agenda_proyecto_final_barroso.Internet;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PrincipalDAO extends DAO<ModeloPrincipal>{

    public PrincipalDAO(Singleton conexion) {
        super(conexion);
    }

    @Override
    public void add(ModeloPrincipal obj) {
        if(Internet.getInternet()){
            
            System.out.println("Nombre: " + obj.getNombre());
            System.out.println("Telefono: " + obj.getTelefono());
            System.out.println("Correo: " + obj.getCorreo());
            System.out.println("Celular: " + obj.getCelular());
            
            try {
                indice = 1;
                
                ps = conexion.obtenerConexion().prepareCall("insert into registros values(0, ?, ?, ?, ?)");
                ps.setString(indice++, obj.getNombre());
                ps.setString(indice++, obj.getTelefono());
                ps.setString(indice++, obj.getCorreo());
                ps.setString(indice++, obj.getCelular());
                
                ps.execute();
            } catch (SQLException e) {
                System.out.println("Error PrincipalDAO: " + e.getMessage());
            }
        }else{
            System.out.println("Sin internet");
        }
    }

    @Override
    public void edit(ModeloPrincipal obj) {}

    @Override
    public void remove(ModeloPrincipal obj) {}

    @Override
    public ResultSet consultar(String consulta) {
        try {
            ps = conexion.obtenerConexion().prepareStatement(consulta);
            rs = ps.executeQuery();
            return rs;
        } catch (SQLException e) {
            Logger.getLogger(PrincipalDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }
    
}
