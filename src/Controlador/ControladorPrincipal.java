package Controlador;

import Factory.Factory;
import Modelo.DAO;
import Modelo.ModeloLista;
import Modelo.ModeloPrincipal;
import Singleton.Singleton;
import Vista.VistaLista;
import Vista.VistaPrincipal;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class ControladorPrincipal implements ActionListener{
    
    /*Declaracion de variables de clase de la vista y el modelo para el correcto funcionamiento de la clase*/
    private final JPanel panel = new JPanel();
    private final VistaPrincipal vista;
    private final ModeloPrincipal modelo;

    /*Constructor que recibira la vista y el modelo para poderlos inicializar para poderlos utilizar en la clase*/
    public ControladorPrincipal(VistaPrincipal vista, ModeloPrincipal modelo) {
        this.vista = vista;
        this.modelo = modelo;
    }
    
    /*Metodo iniciar que inicializara a la vista para que se pueda visualizar*/
    public void iniciar(){
        vista.setTitle("Menu Principal");
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
        asignarControl();
    }

    /*Metodo asignarControl que le dara contexto en la clase para poder realizar eventos con las cosas que esten en la vista*/
    private void asignarControl() {
        vista.getTf_Nombre().addActionListener(this);
        vista.getTf_Telefono().addActionListener(this);
        vista.getTf_Celular().addActionListener(this);
        vista.getTf_Correo().addActionListener(this);
        vista.getBtn_Guardar().addActionListener(this);
        vista.getBtn_Listar().addActionListener(this);
    }

    /*Metodo que atrapara cada uno de los eventos de los componentes que esten en la vista para poderlos utilizar*/
    @Override
    public void actionPerformed(ActionEvent e) {
        /*If que detectara si se ckliqueo en el boton guardar*/
        if(e.getSource() == vista.getBtn_Guardar()){
            
            /*If que verificara que no se manden valores vacios a la base de datos para no tener conflicto, y si van vacias mandar a el usuario
            un mensaje advirtiendole su error*/
            if(vista.getTf_Nombre().getText().equals("")
               || vista.getTf_Correo().getText().equals("") || vista.getTf_Celular().getText().equals("")){
                JOptionPane.showMessageDialog(panel, "Algun campo se encuentra vacio, favor de complementarlos", "Warning", JOptionPane.WARNING_MESSAGE);
            }else{
             
                /*Modificando el evento de disponibilidad para que no se pueda realizar ninguna accion en los elementos que se le aplique*/
                vista.getBtn_Guardar().setEnabled(false);
                vista.getBtn_Listar().setEnabled(false);
                
                try {
                    /*Instancia a la clase factory por medio de la interface DAO, para poder obtener la instancia de la clase que se le este pidiendo
                    y la pueda regresar para poder trabajar con ella*/
                    DAO p = Factory.extender(1, Singleton.crearConexion());

                    /*Asignamos los valores de la vista a la clase modelo por medio de sus Setters para que los pueda guardar y poderlos mandar a el DAO 
                    y poder realizar una accion en la base de datos*/
                    modelo.setNombre(vista.getTf_Nombre().getText());
                    modelo.setTelefono(vista.getTf_Telefono().getText());
                    modelo.setCorreo(vista.getTf_Correo().getText());
                    modelo.setCelular(vista.getTf_Celular().getText());
                    
                    /*Se le manda a llamar a el metodo abstracto add para que realice alguna accion con los datos mandados*/
                    p.add(modelo);
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                /*Vaciando los textFiel para que puedan ingresar valores nuevos*/
                vista.getTf_Nombre().setText("");
                vista.getTf_Telefono().setText("");
                vista.getTf_Correo().setText("");
                vista.getTf_Celular().setText("");
                
                /*Activando de nuevo los botones para que pueda realizar acciones*/
                vista.getBtn_Guardar().setEnabled(true);
                vista.getBtn_Listar().setEnabled(true);
                
            }
        }
        
        /*If que detecta el click del el boton listar*/
        if(e.getSource() == vista.getBtn_Listar()){
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    try {
                        // Instanciando a la clase ControladorLista, mandandole el modelo y la vista, e iniciando el metodo iniciar
                        new ControladorLista(new VistaLista(), new ModeloLista()).iniciar();
                    } catch (ClassNotFoundException | SQLException ex) {
                        Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    /*Evento para poder cerrar la vista actual y poder visualizar solo una vista  ala vez*/
                    vista.dispose();
                }
            });
        }
    }
    
}
